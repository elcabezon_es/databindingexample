# README #


This is an example of DataBinding in android. It has created a new blank project and Android Studio 1.5.1

It has consulted the official documentation android

http://developer.android.com/intl/es/tools/data-binding/guide.html


**To get started:**

First you have to enable databinding element to this add this in build.gradle (app module)
```
#!java
android {
    ....
    dataBinding {
        enabled = true
    }
}
```


Before using binding expressions need to wrap the label layout in the layout (Note that is lowercase ) and add the element data that is " injected " the model to this view.

You can see this in the code of this repository.

[activity_main.xml](https://bitbucket.org/elcabezon_es/databindingexample/src/27ad2890bcbf900a552413380b96916baf8e0e02/app/src/main/res/layout/activity_main.xml?at=master&fileviewer=file-view-default)

Once done you can start using binding expressions


```
#!xml
<TextView
...
android:text='@{user.firstName}'
...
/>

```

Now we can bind data


```
#!java

ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
User user = new User("Test", "User");
binding.setUser(user);

```


This is a basic example of how to use bindingdata in android. In the example you can also see how to make binding events.
