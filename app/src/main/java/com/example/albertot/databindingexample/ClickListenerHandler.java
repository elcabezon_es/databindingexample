package com.example.albertot.databindingexample;


import android.util.Log;
import android.view.View;

public class ClickListenerHandler {

    private User user;

    public ClickListenerHandler(User user) {
        this.user = user;
    }

    public void ClickPruebaUno(View v){
        user.setFirstName("Cambia");
        Log.d("DATABIND","Probando click uno");
    }
    public void ClickPruebaDos(View v){
        user.setLastName("Nombre");
        Log.d("DATABIND","Probando click dos");
    }
}
